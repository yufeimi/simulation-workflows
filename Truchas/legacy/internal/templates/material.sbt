<SMTK_AttributeSystem Version="2">
  <Categories>
    <Cat>Heat Transfer</Cat>
    <Cat>Fluid Flow</Cat>
  </Categories>
  <!-- Attribute Definitions-->
  <Definitions>
    <AttDef Type="background-material" Label="Background Material" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <AttributeRef Name="background-material" Label="Background Material" NumberOfRequiredValues="1">
          <AttDef>material</AttDef>
        </AttributeRef>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="material" Label="Material" BaseType="" Version="0" Unique="true">
      <AssociationsDef Name="MaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <String Name="material-type" Label="Material Type">
          <ChildrenDefinitions>
            <Double Name="density" Label="Density (rho)" Optional="false">
              <BriefDescription>Mass density of the material phase</BriefDescription>
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
              <ExpressionType>tabular-function</ExpressionType>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="density-deviation" Label="Density Deviation" Optional="true" IsEnabledByDefault="false">
              <BriefDescription>The relative deviation of the true temperature-dependent
density from the reference density</BriefDescription>
              <Categories>
                <Cat>Fluid Flow</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
            </Double>
            <Double Name="conductivity" Label="Conductivity (K)">
              <BriefDescription>Thermal conductivity of the material phase</BriefDescription>
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
              <ExpressionType>tabular-function</ExpressionType>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="viscosity" Label="Viscosity (nu)">
              <BriefDescription>The dynamic viscosity of a fluid phase</BriefDescription>
              <Categories>
                <Cat>Fluid Flow</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
              <ExpressionType>tabular-function</ExpressionType>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
            <String Name="thermal" Label="Thermal Specification">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <ChildrenDefinitions>
                <Double Name="specific-heat" Label="Specific Heat (Cp)">
                  <BriefDescription>Specific heat of the material phase</BriefDescription>
                  <DefaultValue>0.0</DefaultValue>
                  <ExpressionType>tabular-function</ExpressionType>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                  </RangeInfo>
                </Double>
                <Double Name="enthalpy" Label="Enthalpy (E)">
                  <DefaultValue>0.0</DefaultValue>
                  <ExpressionType>tabular-function</ExpressionType>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                  </RangeInfo>
                </Double>
              </ChildrenDefinitions>
              <DiscreteInfo DefaultIndex="0">
                <Structure>
                  <Value Enum="Specific Heat">specific-heat</Value>
                  <Items>
                    <Item>specific-heat</Item>
                  </Items>
                </Structure>
                <Structure>
                  <Value Enum="Enthalpy">enthalpy</Value>
                  <Items>
                    <Item>enthalpy</Item>
                  </Items>
                </Structure>
              </DiscreteInfo>
            </String>
            <String Name="thermal-two-phase" Label="Thermal Specification">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <ChildrenDefinitions>
                <Double Name="specific-heat" Label="Specific Heat (Cp)">
                  <BriefDescription>Specific heat of the material phase</BriefDescription>
                  <DefaultValue>0.0</DefaultValue>
                  <ExpressionType>tabular-function</ExpressionType>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                  </RangeInfo>
                </Double>
                <Double Name="latent-heat" Label="Latent Heat (Lf)">
                  <BriefDescription>The latent heats of the phase transformations.</BriefDescription>
                  <DefaultValue>0.0</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                  </RangeInfo>
                </Double>
                <Double Name="enthalpy" Label="Enthalpy (E)">
                  <DefaultValue>0.0</DefaultValue>
                  <ExpressionType>tabular-function</ExpressionType>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                  </RangeInfo>
                </Double>
              </ChildrenDefinitions>
              <DiscreteInfo DefaultIndex="0">
                <Structure>
                  <Value Enum="Specific Heat">specific-heat</Value>
                  <Items>
                    <Item>specific-heat</Item>
                    <Item>latent-heat</Item>
                  </Items>
                </Structure>
                <Structure>
                  <Value Enum="Enthalpy">enthalpy</Value>
                  <Items>
                    <Item>enthalpy</Item>
                  </Items>
                </Structure>
              </DiscreteInfo>
            </String>
            <!-- Two phase properties-->
            <Double Name="solid-transition-temperature" Label="Solidus Temperature (Ts)">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
            </Double>
            <Double Name="liquid-transition-temperature" Label="Liquidus Temperature (Tl)">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DefaultValue>100.0</DefaultValue>
            </Double>
            <!-- Void material properties-->
            <Double Name="void-temperature" Label="Temperature">
              <Categories>
                <Cat>Fluid Flow</Cat>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
            </Double>
            <Double Name="sound-speed" Label="Speed of Sound" AdvanceLevel="1">
              <BriefDescription>The adiabatic sound speed that is used in computing the
compressibility of each cell containing the material.
This is not a real sound speed, but a numerical artifice
used to permit collapse of small void bubbles.</BriefDescription>
              <DefaultValue>0.0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="2">
            <Structure>
              <Value Enum="Solid">solid</Value>
              <Items>
                <Item>density</Item>
                <Item>density-deviation</Item>
                <Item>conductivity</Item>
                <Item>thermal</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Fluid">fluid</Value>
              <Items>
                <Item>density</Item>
                <Item>density-deviation</Item>
                <Item>conductivity</Item>
                <Item>viscosity</Item>
                <Item>thermal</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Two Phase">two-phase</Value>
              <Items>
                <Item>density</Item>
                <Item>density-deviation</Item>
                <Item>conductivity</Item>
                <Item>viscosity</Item>
                <Item>thermal-two-phase</Item>
                <Item>solid-transition-temperature</Item>
                <Item>liquid-transition-temperature</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Void">void</Value>
              <Items>
                <Item>void-temperature</Item>
                <Item>sound-speed</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeSystem>