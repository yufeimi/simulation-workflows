<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">
  <Categories>
    <Cat>Heat Transfer</Cat>
    <Cat>Fluid Flow</Cat>
  </Categories>
  <Definitions>
    <!-- Boundary Conditions-->
    <AttDef Type="boundary-condition" Label="Boundary Condition (&amp;BC)" BaseType="" Version="0" Unique="true">
      <AssociationsDef Name="ModelAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <String Name="variable" Label="Variable">
          <ChildrenDefinitions>
            <String Name="pressure-bc-type" Label="Boundary Condition Type">
              <Categories>
                <Cat>Fluid Flow</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Dirichlet">dirichlet</Value>
              </DiscreteInfo>
            </String>
            <String Name="velocity-bc-type" Label="Boundary Condition Type">
              <Categories>
                <Cat>Fluid Flow</Cat>
              </Categories>
              <ChildrenDefinitions>
                <Group Name="velocity-group" Label="Velocity" Extensible="true" NumberOfRequiredGroups="1">
                  <ItemDefinitions>
                    <Double Name="velocity-value" Label="Velocity" NumberOfRequiredValues="4">
                      <ComponentLabels>
                        <Label>t</Label>
                        <Label>u</Label>
                        <Label>v</Label>
                        <Label>w</Label>
                      </ComponentLabels>
                      <DefaultValue>0.0</DefaultValue>
                    </Double>
                  </ItemDefinitions>
                </Group>
              </ChildrenDefinitions>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Free Slip">free-slip</Value>
                <Value Enum="No Slip">no-slip</Value>
                <Structure>
                  <Value Enum="Dirichlet">dirichlet</Value>
                  <Items>
                    <Item>velocity-group</Item>
                  </Items>
                </Structure>
              </DiscreteInfo>
            </String>
            <Double Name="pressure-value" Label="Value">
              <Categories>
                <Cat>Fluid Flow</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
              <ExpressionType>tabular-function</ExpressionType>
            </Double>
            <Group Name="inflow" Label="Inflow" Optional="true" IsEnabledByDefault="false">
              <ItemDefinitions>
                <AttributeRef Name="inflow-material" Label="Inflow Material" NumberOfRequiredValues="1">
                  <AttDef>material</AttDef>
                  <Categories>
                    <Cat>Fluid Flow</Cat>
                  </Categories>
                </AttributeRef>
                <Double Name="inflow-temperature" Label="Inflow Temperature">
                  <Categories>
                    <Cat>Fluid Flow</Cat>
                  </Categories>
                  <DefaultValue>0.0</DefaultValue>
                </Double>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Pressure">pressure</Value>
              <Items>
                <Item>pressure-bc-type</Item>
                <Item>pressure-value</Item>
                <Item>inflow</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Velocity">velocity</Value>
              <Items>
                <Item>velocity-bc-type</Item>
                <Item>inflow</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeSystem>
