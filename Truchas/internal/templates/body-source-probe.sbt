<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Categories>
    <Cat>Fluid Flow</Cat>
    <Cat>Heat Transfer</Cat>
    <Cat>Induction Heating</Cat>
    <Cat>Solid Mechanics</Cat>
  </Categories>
  <!-- Attribute Definitions-->
  <Definitions>
    <!-- Note: BODY attribute replaced by standalone initial conditions defintiions (December 2018)-->
    <!-- DS_SOURCE-->
    <AttDef Type="ht.source" Label="Volumetric Heat Source" BaseType="" Version="0">
      <AssociationsDef Name="DSSourceAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="source" Label="Temperature">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ExpressionType>tabular-function</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <!-- PROBE-->
    <AttDef Type="probe" Label="Probe" BaseType="" Version="0">
      <ItemDefinitions>
        <String Name="description" Label="Description" MultipleLInes="true" Optional="true" IsEnabledByDefault="false"></String>
        <Double Name="coords" Label="Coordinates" NumberOfRequiredValues="3">
          <BriefDescription>Used to define the location of the probe</BriefDescription>
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ComponentLabels>
            <Label>x:</Label>
            <Label>y:</Label>
            <Label>z:</Label>
          </ComponentLabels>
        </Double>
        <Double Name="scale" Label="Scale">
          <DefaultValue>1.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
