<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Categories>
    <Cat>Heat Transfer</Cat>
  </Categories>
  <!-- Attribute definitions for item-specific functions-->
  <Definitions>
    <AttDef Type="fn.material" BaseType="" Abstract="true" Version="0"></AttDef>
    <AttDef Type="fn.initial-condition" BaseType="" Abstract="true" Version="0"></AttDef>
    <!-- Thermal Conductivity function of Temp-->
    <AttDef Type="fn.material.conductivity" BaseType="fn.material" Label="Conductivity" RootName="ConductivityFn" Version="0">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0" stuff="Heat Transfer">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Temp" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
                <Double Name="Value" Label="Conductivity" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Temperature">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Coefficient" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
                <Int Name="Value" Label="Exponent (Temp)" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <!-- Density deviation function of Temp-->
    <AttDef Type="fn.material.density-deviation" BaseType="fn.material" Label="Density Deviation" RootName="DensityDeviationFn" Version="0">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0" stuff="Heat Transfer">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Temp" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
                <Double Name="Value" Label="Density Deviation" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Temperature">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Coefficient" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
                <Int Name="Value" Label="Exponent (Temp)" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <!-- Specific Heat function of Temp-->
    <AttDef Type="fn.material.specific-heat" BaseType="fn.material" Label="Specific Heat" RootName="SpecificHeatFn" Version="0">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0" stuff="Heat Transfer">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Temp" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
                <Double Name="Value" Label="Specific Heat" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Temperature">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Coefficient" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
                <Int Name="Value" Label="Exponent (Temp)" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <!-- Viscosity function of Temp-->
    <AttDef Type="fn.material.viscosity" BaseType="fn.material" Label="Viscosity" RootName="ViscosityFn" Version="0">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0" stuff="Heat Transfer">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Temp" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
                <Double Name="Value" Label="Viscosity" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Center Temperature">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Coefficient" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
                <Int Name="Value" Label="Exponent (Temp)" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <!-- Initial Temperature function of x,y,z, or all 3-->
    <AttDef Type="fn.initial-condition.temperature" BaseType="fn.initial-condition" Abstract="true" Version="0"></AttDef>
    <AttDef Type="fn.initial-condition.temperature.x" BaseType="fn.initial-condition.temperature" Label="f(x) Initial Temperature" RootName="TemperatureFn" Version="0">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0" stuff="Heat Transfer">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="X" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
                <Double Name="Value" Label="Temperature" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Initial Temperature">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Coefficient" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
                <Int Name="Value" Label="Exponent (X)" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.initial-condition.temperature.y" BaseType="fn.initial-condition.temperature" Label="f(y) Initial Temperature" RootName="TemperatureFn" Version="0">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0" stuff="Heat Transfer">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Y" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
                <Double Name="Value" Label="Temperature" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Initial Temperature">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Coefficient" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
                <Int Name="Value" Label="Exponent (Y)" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.initial-condition.temperature.z" BaseType="fn.initial-condition.temperature" Label="f(z) Initial Temperature" RootName="TemperatureFn" Version="0">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0" stuff="Heat Transfer">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Group Name="tabular-data" Label="Tabular Data" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Z" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
                <Double Name="Value" Label="Temperature" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
              </ItemDefinitions>
            </Group>
            <Double Name="center" Label="Initial Temperature">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="X" Label="Coefficient" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
                <Int Name="Value" Label="Exponent (Z)" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Tabular">tabular</Value>
              <Items>
                <Item>interpolation</Item>
                <Item>extrapolation</Item>
                <Item>tabular-data</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="fn.initial-condition.temperature.xyz" BaseType="fn.initial-condition.temperature" Label="f(x,y,z) Initial Temperature" RootName="TemperatureFn" Version="0">
      <ItemDefinitions>
        <String Name="type" Label="Function Type" Version="0">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ChildrenDefinitions>
            <String Name="interpolation" Label="Interpolation Method" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value>linear</Value>
                <Value>akima</Value>
              </DiscreteInfo>
            </String>
            <String Name="extrapolation" Label="Extrapolation Method" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value>nearest</Value>
                <Value>linear</Value>
              </DiscreteInfo>
            </String>
            <Double Name="center" Label="Center Coordinates" NumberOfRequiredValues="3">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Group Name="polynomial-terms" Label="Polynomial Terms f(x,y,z)" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="Coefficient" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Double>
                <Int Name="Exponent" Label="Exponents (x,y,z)" NumberOfRequiredValues="3">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <ComponentLabels>
                    <Label>X</Label>
                    <Label>Y</Label>
                    <Label>Z</Label>
                  </ComponentLabels>
                  <DefaultValue>0</DefaultValue>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Polynomial">polynomial</Value>
              <Items>
                <Item>center</Item>
                <Item>polynomial-terms</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
