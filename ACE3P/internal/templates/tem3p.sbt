<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">
  <!-- **********  Include files ***********-->
  <!-- Typically one include file with definitions per tab-->
  <Includes>
    <File>internal/templates/tem3p-boundarycondition.sbt</File>
    <File>internal/templates/tem3p-material.sbt</File>
    <File>internal/templates/tem3p-mesh.sbt</File>
    <File>internal/templates/tem3p-solver.sbt</File>
    <File>internal/templates/tem3p-thermal.sbt</File>
  </Includes>

  <Views>
    <!-- View(Type="Group" Title="TEM3P" TopLevel=toplevel TabPosition="North"-->
    <!--     FilterByAdvanceLevel="false")-->
    <!--   Views-->
    <!--     View(Title="TEM3P Mechanical BC")/-->
    <!--     View(Title="TEM3P Thermal BC")/-->
    <!--     View(Title="TEM3P Elastic Material")/-->
    <!--     View(Title="TEM3P Thermal Material")/-->
    <!--     View(Title="TEM3P Elastic Analysis")/-->
    <!--     View(Title="TEM3P Thermal Analysis")/-->
    <!--     View(Title="TEM3P Mesh Output")/-->
    <!--     View(Title="TEM3P Thermal Shells")/-->
    <!--     View(Title="Heat Sources")-->

    <View Type="ModelEntity" Title="TEM3P Mechanical BC" Label="Boundary Conditions" ModelEntityFilter="f" ColHeader2="Boundary Condition" NoValueLabel="-please-select-">
      <AttributeTypes>
        <Att Type="TEM3PMechanicalBC"></Att>
      </AttributeTypes>
    </View>
    <View Type="ModelEntity" Title="TEM3P Thermal BC" Label="Boundary Conditions" ModelEntityFilter="f" ColHeader2="Boundary Condition" NoValueLabel="-please-select-">
      <AttributeTypes>
        <Att Type="TEM3PThermalBC"></Att>
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="TEM3P Elastic Material" Label="Materials">
      <AttributeTypes>
        <Att Type="TEM3PElasticMaterial"></Att>
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="TEM3P Thermal Material" Label="Materials">
      <AttributeTypes>
        <Att Type="TEM3PThermalMaterial"></Att>
      </AttributeTypes>
    </View>
    <!-- Need separate elastic and thermal analysis attributes (for ThermoElastic)-->
    <View Type="Instanced" Name="TEM3P Elastic Analysis" Label="Analysis">
      <InstancedAttributes>
        <Att Name="ElasticOrderAtt" Type="Order"/>
        <Att Name="ElasticEigenSolverAtt" Type="EigenSolver"/>
        <Att Name="ElasticLinearSolverAtt" Type="TEM3PLinearSolver"/>
        <Att Name="ElasticHarmonicAnalysisAtt" Type="HarmonicAnalysis"/>
        <Att Name="ElasticNonlinearSolverAtt" Type="NonlinearSolver"/>
        <Att Name="ElasticPicardSolverAtt" Type="PicardSolver"/>
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Name="TEM3P Thermal Analysis" Label="Analysis">
      <InstancedAttributes>
        <Att Name="ThermalOrderAtt" Type="Order"/>
        <Att Name="ThermalEigenSolverAtt" Type="EigenSolver"/>
        <Att Name="ThermalLinearSolverAtt" Type="TEM3PLinearSolver"/>
        <Att Name="ThermalHarmonicAnalysisAtt" Type="HarmonicAnalysis"/>
        <Att Name="ThermalNonlinearSolverAtt" Type="NonlinearSolver"/>
        <Att Name="ThermalPicardSolverAtt" Type="PicardSolver"/>
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="Mesh Output">
      <InstancedAttributes>
        <Att Name="MeshDump" Type="MeshDump"></Att>
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Name="Thermal Shells" Label="Shells">
      <AttributeTypes>
        <Att Type="ThermalShell"></Att>
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Heat Sources">
      <AttributeTypes>
        <Att Type="HeatSource"></Att>
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeSystem>
